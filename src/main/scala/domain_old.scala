import scala.collection.mutable

object domain_old {
  class Container {
    val destinationType: DestinationType
  }

  class Storage {
    private val queue: mutable.Queue[Container]

    def getNext: Container = queue.dequeue()

    def isEmpty: Boolean = queue.isEmpty
  }

  class Transport {
    val transportType: TransportType
    val cargo: Option[Container]
    val state: TransportState
  }

  class TransportState {
    val source: Storage
    val destination: Storage
    val remainSteps: Int
  }

  sealed trait DestinationType

  object ADestination extends DestinationType

  object BDestination extends DestinationType

  sealed trait TransportType

  object Car extends TransportType

  object Boat extends TransportType
}
