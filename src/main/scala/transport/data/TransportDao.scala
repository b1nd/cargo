package transport.data

import domain.transport.{Transport, TransportId}

import scala.collection.mutable

trait TransportDao {
  def getAll: Seq[Transport]
  def get(id: TransportId): Transport
  def save(transport: Transport): Transport
}

class TransportDaoImpl extends TransportDao {
  private val map: mutable.Map[TransportId, Transport] = mutable.Map.empty

  override def getAll: Seq[Transport] = ???

  override def get(id: TransportId): Transport = map(id)

  override def save(transport: Transport): Transport = ???
}
