package transport.domain

import domain.cargo.Cargo
import domain.transport.{Transport, TransportId}
import domain.world.Time
import transport.data.TransportDao

trait TransportService {
  def getAll: Seq[Transport]

  // todo: set path (swap), path time, cargo
  def loadNewCargo(transportId: TransportId, cargo: Cargo): Transport

  // locationService.getTransportPath(transport)
  // maybe Transport instead of TransportId?
  def unloadCargo(transportId: TransportId): Transport

  // update entity.remainTime - tick
  def timePass(transportId: TransportId, tick: Time): Transport
}

// Если используем модули в архитектуре, то модуль api (который мы подключаем в другие модули)
// содержит содержит только интерфейс TransportService, без реализации (Чтобы не тянуть зависимости библиотек, а только домен).
// А модуль для реализаций содержит реализацию TransportServiceImpl
class TransportServiceImpl(transportDao: TransportDao)
    extends TransportService {

  // Если строго следуем архетиктурному шаблону - то проксируем, т.к. Dao не domain область
  // Иначе можно использовать напрямую TransportDao.getAll вместо TransportService.getAll
  override def getAll: Seq[Transport] = transportDao.getAll

  override def loadNewCargo(transportId: TransportId, cargo: Cargo): Transport =
    ???

  override def unloadCargo(transportId: TransportId): Transport = ???

  override def timePass(transportId: TransportId, tick: Time): Transport = {
    val transport = transportDao.get(transportId)
    val updated   =
      transport.copy(remainTime = Time(transport.remainTime.time - tick.time))
    transportDao.save(updated)
  }
}
