package world.domain

import cargo.domain.CargoService
import domain.world.{Event, Time}
import transport.domain.TransportService

trait World {
  def now: Time

  def advance(): Seq[Event]
}

class WorldImpl(tick: Int)(
  transportService: TransportService,
  cargoService: CargoService
) extends World {
  private var time: Int = 0

  override def now: Time = Time(time)

  override def advance(): Seq[Event] = {
    transportService.getAll.foreach { transport =>
      if (transport.remainTime.time == 0) {
        transport.container match {
          case Some(cargo) =>
            transportService.unloadCargo(transport.id)
            cargoService.supply(transport.path.to, cargo.finalDestination)
          case None        =>
            val newCargo = cargoService.getCargo(transport.path.to)
            newCargo match {
              case Some(cargo) =>
                transportService.loadNewCargo(transport.id, cargo)
              case None        => ()
            }
        }
      }
      transport
    }
    time += tick
  }
}
