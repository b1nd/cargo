package cargo.domain

import domain.cargo.Cargo
import domain.location.LocationId

trait CargoService {
  def isAllDelivered: Boolean
  def supply(at: LocationId, finalDestination: LocationId): Cargo
  def getCargo(from: LocationId): Option[Cargo]
}
