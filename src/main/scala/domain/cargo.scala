package domain

import domain.location.LocationId

object cargo {
  case class CargoId(id: Long)

  case class Cargo(finalDestination: LocationId)
}
