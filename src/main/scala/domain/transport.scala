package domain

import domain.cargo.Cargo
import domain.location.Path
import domain.world.Time

object transport {
  case class TransportId(id: Long)

  sealed trait TransportType

  case object Car extends TransportType

  case object Boat extends TransportType

  case class Transport(
    id: TransportId,
    transportType: TransportType,
    container: Option[Cargo],
    path: Path,
    remainTime: Time
  )
}
