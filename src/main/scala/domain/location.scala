package domain

import domain.world.Time

object location {
  case class LocationId(id: String)

  case class Path(from: LocationId, to: LocationId)

  case class PathWithTime(path: Path, pathTime: Time)
}
