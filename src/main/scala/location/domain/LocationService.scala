package location.domain

import domain.location.{Path, PathWithTime}
import domain.transport.Transport
import domain.world.Time

trait LocationService {
  def getPathTime(path: Path): Time

  def getTransportPath(transport: Transport): PathWithTime
}
